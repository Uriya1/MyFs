#pragma once

#include <string>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdexcept>
#include <errno.h>

class BlockDeviceSimulator
{
	public:
		BlockDeviceSimulator(std::string fname);
		~BlockDeviceSimulator();

		void read(int addr, int size, char *ans);
		void write(int addr, int size, const char *data);

		static const int DEVICE_SIZE = 1024 * 1024;

	private:
		int fd;
		unsigned char* filemap;
};