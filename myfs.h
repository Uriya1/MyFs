#pragma once

#include "blkdev.h"

#include <memory>
#include <vector>
#include <stdint.h>
#include <string.h>
#include <string>
#include <iostream>
#include <math.h>
#include <sstream>
#include <map>

using std::runtime_error;

using std::vector;
using std::atoi;
using std::stoi;
using std::string;
using std::cout;
using std::endl;
using std::map;
using std::to_string;

#define FILE_LIMIT 70
#define INTEGER_BYTES_SIZE 4

#define START_FILE_AMOUNT 0
#define START_FILE_NODES 50
#define START_FILES_DATA 1000

#define START_FILES_AMOUNT_ADDRESS sizeof(myfs_header) + 10
#define START_FILE_NODES_ADDRESS sizeof(myfs_header) + 15
#define START_FILES_DATA_CURSOR_ADDRESS sizeof(myfs_header) + 20

class MyFs
{
	public:
        // C'tor
		MyFs(BlockDeviceSimulator* blockDeviceSimulator);

        // D'tor
		~MyFs(void);

		/**
		 * dir_list_entry struct
		 * This struct is used by list_dir method to return directory entry
		 * information.
		 */
		struct File
		{
			/**
			 * The directory entry name
			 */
			string name;

			/**
			 * whether the entry is a file or a directory
			 */
			bool isDir;

			/**
			 * File Start Address
			 */
			 int startAddress;

            /**
            * File size
            */
            int fileSize;
		};

		typedef vector<struct File> FileList;

		/**
		 * format method
		 * This function discards the current content in the blockdevice and
		 * create a fresh new MYFS instance in the blockdevice.
		 */
		void format(void);

		/**
		 * create_file method
		 * Creates a new file in the required path.
		 * @param path the file path (e.g. "/newfile")
		 * @param isDir boolean indicating whether this is a file or directory
		 */
		void createFile(string path, bool isDir);

		/**
		 * get_content method
		 * Returns the whole content of the file indicated by path_str param.
		 * Note: this method assumes path_str refers to a file and not a
		 * directory.
		 * @param path the file path (e.g. "/somefile")
		 * @return the content of the file
		 */
		string getContent(string path);

		/**
		 * set_content method
		 * Sets the whole content of the file indicated by path_str param.
		 * Note: this method assumes path_str refers to a file and not a
		 * directory.
		 * @param path the file path (e.g. "/somefile")
		 * @param content the file content string
		 */
		void setContent(string path, string content);

		/**
		 * list_dir method
		 * Returns a list of a files in a directory.
		 * Note: this method assumes path_str refers to a directory and not a
		 * file.
		 * @param path the file path (e.g. "/somedir")
		 * @return a vector of dir_list_entry structures, one for each file in
		 *	the directory.
		 */
        FileList list(string path);

	private:
        // Checking file existence
        bool exists(string fileName) const;

        // Getting file instance by name
        File getFileByName(string fileName) const;

        // Saving
        void saveAll(void) const;
        void saveFileNodes(void) const;
        void saveFileDataCursor(void) const;
        void saveFilesAmount(void) const;

        // Loading
        void loadFiles(void);
        File loadFileByAddress(int address);

		/**
		 * This struct represents the first bytes of a myfs filesystem.
		 * It holds some magic characters and a number indicating the version.
		 * Upon class construction, the magic and the header are tested - if
		 * they both exist than the file is assumed to contain a valid myfs
		 * instance. Otherwise, the blockdevice is formated and a new instance is
		 * created.
		 */
		struct myfs_header
		{
			char magic[4];
			uint8_t version;
		};

		BlockDeviceSimulator* _blockDeviceSimulator;
		FileList _files;

		int _filesAmount;
		int _fileNodes;
		int _filesDataCursor;

		static const uint8_t CURR_VERSION = 0x03;
		static const char *MYFS_MAGIC;
};