#include "myfs.h"

const char *MyFs::MYFS_MAGIC = "MYFS";

MyFs::MyFs(BlockDeviceSimulator* blockDeviceSimulator)
	: _blockDeviceSimulator(blockDeviceSimulator), _files(FileList()), 
		_filesAmount(-1), // Files Amount
		_fileNodes(-1), // Files Addresses
		_filesDataCursor(-1) // Files Data Addresses
{
	struct myfs_header header;
	this -> _blockDeviceSimulator -> read(0, sizeof(header), (char*) &header);

	if (strncmp(header.magic, MYFS_MAGIC, sizeof(header.magic)) != 0 ||
	    (header.version != CURR_VERSION))
	{
		cout << "Did not find myfs instance on blkdev" << endl;
		cout << "Creating..." << endl;

		// Formatting File
		format();

		cout << "Finished!" << endl;
	}

	// Initializing Existed Files
	else
	{
		cout << "Opening" << endl;
		char buffer[INTEGER_BYTES_SIZE];

		// Getting _filesAmount value
		this -> _blockDeviceSimulator -> read(START_FILES_AMOUNT_ADDRESS, INTEGER_BYTES_SIZE, (char*) buffer);
		this -> _filesAmount = atoi(buffer);

		// Getting _fileNodes value
		this -> _blockDeviceSimulator -> read(START_FILE_NODES_ADDRESS, INTEGER_BYTES_SIZE, (char*) buffer);
		this -> _fileNodes = atoi(buffer);

		// Getting _filesDataCursor value
		this -> _blockDeviceSimulator -> read(START_FILES_DATA_CURSOR_ADDRESS, INTEGER_BYTES_SIZE, (char*) buffer);
		this -> _filesDataCursor = atoi(buffer);

	cout << "OPEN: \n_filesAmount: " << _filesAmount << "\n_filesDataCursor: " << _filesDataCursor 
			<< "\n_fileNodes: " << _fileNodes <<  endl;	

			loadFiles();
	}
}

MyFs::~MyFs(void)
{
	cout << "Closing" << endl;

	// Releasing Memory
	delete(this -> _blockDeviceSimulator);
	this -> _files.clear();
}

void MyFs::format(void)
{
	// Adding custom MYFS Header
	struct myfs_header header;
	strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
	header.version = CURR_VERSION;

	// Formatting
	this -> _blockDeviceSimulator -> write(0, sizeof(header), (const char*) &header);

	this -> _files.clear();

	// Setting Up
	this -> _filesAmount = START_FILE_AMOUNT;
	this -> _fileNodes = START_FILE_NODES;
	this -> _filesDataCursor = START_FILES_DATA;

	cout << "FORMAT: \n_filesAmount: " << _filesAmount << "\n_filesDataCursor: " << _filesDataCursor 
		<< "\n_fileNodes: " << _fileNodes <<  endl;

	// Saving in file
	saveAll();
}

void MyFs::createFile(string path, bool isDir)
{
	// No Directory Support
	if (isDir)
	{
		throw runtime_error("Can't handle directories, yet.");
	}

	else if (exists(path))
	{
		throw runtime_error(path + " already exists!");
	}

	// Creating File Entry (size=0)
	File file = {path, isDir, this -> _filesDataCursor, 0};

	// Saving File Locally
	this -> _files.push_back(file);


	// Saving File Details
	this -> _blockDeviceSimulator -> write(this -> _fileNodes, sizeof(File), 
		(const char*) &file);

	// Updating Cursors
	this -> _filesAmount += 1;
	
	saveAll();
}

string MyFs::getContent(string path)
{
	if (!exists(path))
	{
		throw runtime_error(path + " doesn't exist!");
	}

	File file = getFileByName(path);

	char buffer[FILE_LIMIT];

	if (file.fileSize == 0)
	{
		return "";
	}

	// Reading into the buffer and returning it
	this -> _blockDeviceSimulator -> read(file.startAddress, file.fileSize, buffer);

	return to_string(buffer);
}

void MyFs::setContent(string path, string content)
{
	if (!exists(path))
	{
		throw runtime_error(path + " doesn't exist!");
	}

	File file = getFileByName(path);

	// Updating Size
	file.fileSize = sizeof(content);

	// Updating file data
	this -> _blockDeviceSimulator -> write(file.startAddress, 
		file.fileSize, content.c_str());
}

MyFs::FileList MyFs::list(string path)
{
	return this -> _files;
}

MyFs::File MyFs::getFileByName(string fileName) const
{
	if (!exists(fileName))
	{
		throw runtime_error(fileName + " doesn't exist!");
	}

	for (const auto& file: this -> _files)
	{
		if (file.name == fileName)
		{
			return file;
		}
	}

	throw runtime_error(fileName + " could not be found!");
}

bool MyFs::exists(string fileName) const
{
	for (const auto& file: this -> _files)
	{
		if (file.name == fileName)
		{
			// Found a file with the certain name
			return true;
		}
	}

	return false;
}

void MyFs::saveAll(void) const
{
	saveFilesAmount();
	saveFileNodes();
	saveFileDataCursor();
}

void MyFs::saveFileNodes(void) const
{
	// Saving File Nodes
	this -> _blockDeviceSimulator -> write(START_FILE_NODES_ADDRESS, INTEGER_BYTES_SIZE, 
		to_string(this -> _fileNodes).c_str());
}

void MyFs::saveFileDataCursor(void) const
{
	// Saving _filesDataCursor value
	this -> _blockDeviceSimulator -> write(START_FILES_DATA_CURSOR_ADDRESS, INTEGER_BYTES_SIZE, 
		to_string(this -> _filesDataCursor).c_str());
}

void MyFs::saveFilesAmount(void) const
{
	// Saving _filesDataCursor value
	this -> _blockDeviceSimulator -> write(START_FILES_AMOUNT_ADDRESS, INTEGER_BYTES_SIZE, 
		to_string(this -> _filesAmount).c_str());
}

void MyFs::loadFiles(void)
{
	this -> _files.clear();

	File file;
	int current = START_FILE_NODES;

	for (int i = 0; i < this -> _filesAmount; i++)
	{
		// Getting File Details
		this -> _blockDeviceSimulator -> read(current, sizeof(File), (char*) &file);

		// Adding to local files
		this -> _files.push_back(file);

		// Going for the next file
		current += sizeof(File) + 1;
	}
}